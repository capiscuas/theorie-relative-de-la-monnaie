#!/usr/bin/env bash

VERSION=$1

current_dir=$(pwd)
cd build;

# zip the french build folder for release
zip -r TRM_fr_FR_${VERSION}.zip fr_FR

# zip the english build folder for release
zip -r TRM_en_US_${VERSION}.zip en_US

cd ${current_dir};
