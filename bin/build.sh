#!/usr/bin/env bash

# build french version
./bin/build_fr_FR.sh

status=$?
[ $status -eq 0 ] && echo "" || exit 1

# build english version
./bin/build_en_US.sh

status=$?
[ $status -eq 0 ] && echo "" || exit 1
