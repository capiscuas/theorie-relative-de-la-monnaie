#!/usr/bin/env python
import re
from os import scandir
from pathlib import Path
import logging

logging.basicConfig(level=logging.INFO)
SOURCE_PATH = Path(__file__).parents[1].joinpath("source")
BUILD_PATH = Path(__file__).parents[1].joinpath("build")
EPUB_PDF_FILENAME = "TheorieRelativedelaMonnaie"
HTML_BUILD_PATH = BUILD_PATH.joinpath("html")
EPUB_BUILD_PATH = BUILD_PATH.joinpath("html")
LATEX_BUILD_PATH = BUILD_PATH.joinpath("latex")


def count_euro_sign(content: str) -> int:
    """
    Count number of occurence of € character

    :param content: UTF-8 Text content
    :return:
    """
    re_euro = re.compile(r'(€+)', re.MULTILINE)
    match = re_euro.findall(content)
    return len(match)


def count_euro_latex(content: str) -> int:
    """
    Count number of occurence of \euro latex command

    :param content: UTF-8 Text content
    :return:
    """
    re_latex_euro = re.compile(r'(\\euro)+', re.MULTILINE)
    match = re_latex_euro.findall(content)
    return len(match)


def count_texteuro_latex(content: str) -> int:
    """
    Count number of occurence of \texteuro latex command

    :param content: UTF-8 Text content
    :return:
    """
    re_latex_euro = re.compile(r'(\\texteuro)+', re.MULTILINE)
    match = re_latex_euro.findall(content)
    return len(match)


def count_multiply(content: str) -> int:
    """
    Count number of occurence of × character

    :param content: UTF-8 Text content
    :return:
    """
    re_multiply = re.compile(r'(×+)', re.MULTILINE)
    match = re_multiply.findall(content)
    return len(match)


def count_times(content: str) -> int:
    """
    Count number of occurence of \times latex command

    :param content: UTF-8 Text content
    :return:
    """
    re_latex_times = re.compile(r'(\\times)+', re.MULTILINE)
    match = re_latex_times.findall(content)
    return len(match)


if not Path(BUILD_PATH).is_dir():
    logging.error("Echec: dossier %s non trouvé.", BUILD_PATH)
    exit(1)

sources = list(scandir(SOURCE_PATH))
rst_total_multiply = 0
rst_total_euro = 0
error_found = False
for entry in sources:
    entry_path = Path(entry)
    if entry_path.is_file() and entry_path.suffix == ".rst":
        logging.info("Check %s...", entry_path)
        with open(entry_path, "r", encoding="utf-8") as fh:
            rst_content = fh.read()
            # count multiply
            rst_count_multiply = count_multiply(rst_content)
            rst_count_times = count_times(rst_content)
            rst_total_multiply += (rst_count_multiply + rst_count_times)

            # count euro
            rst_count_euro_sign = count_euro_sign(rst_content)
            rst_count_euro_latex = count_euro_latex(rst_content)
            rst_total_euro += (rst_count_euro_sign + rst_count_euro_latex)

            # Check HTML build
            html_path = HTML_BUILD_PATH.joinpath(entry_path.stem + ".html")
            with open(html_path, "r", encoding="utf-8") as fh2:
                html_content = fh2.read()
                html_count_multiply = count_multiply(html_content)
                html_count_times = count_times(html_content)
                html_count_euro_sign = count_euro_sign(html_content)
                html_count_euro_latex = count_euro_latex(html_content)

                # print(rst_count_multiply + rst_count_times, html_count_multiply + html_count_times)
                if (rst_count_multiply + rst_count_times) != html_count_multiply + html_count_times:
                    logging.error("Echec: %s signes × manquants dans %s.",
                                  abs(html_count_multiply - (rst_count_multiply + rst_count_times)), html_path)
                    error_found = True
                # print(rst_count_euro_sign + rst_count_euro_latex, html_count_euro_sign + html_count_euro_latex)
                if (rst_count_euro_sign + rst_count_euro_latex) != html_count_euro_sign + html_count_euro_latex:
                    logging.error("Echec: %s signes € manquants dans %s.",
                                  abs(html_count_euro_sign - (rst_count_euro_sign + rst_count_euro_latex)), html_path)
                    error_found = True

if error_found:
    logging.error("Check builds...ERREUR !")
    exit(1)

# Check latex build
latex_path = LATEX_BUILD_PATH.joinpath("", EPUB_PDF_FILENAME + ".tex")
with open(latex_path, "r", encoding="utf-8") as fh:
    latex_content = fh.read()
    # count multiply
    latex_count_multiply = count_multiply(latex_content)
    latex_count_times = count_times(latex_content)
    # count euro commands
    latex_count_texteuro_latex = count_texteuro_latex(latex_content)
    latex_count_euro_latex = count_euro_latex(latex_content)

    # print(latex_count_multiply, rst_total_multiply)
    if latex_count_multiply + latex_count_times != rst_total_multiply:
        logging.error("Echec: %s signes × manquants dans %s.",
                      abs(rst_total_multiply - (latex_count_multiply + latex_count_times)), latex_path)
        exit(1)

    # print(rst_total_euro, latex_count_texteuro_latex + latex_count_euro_latex)
    if latex_count_texteuro_latex + latex_count_euro_latex != rst_total_euro:
        logging.error("Echec: %s signes × manquants dans %s.",
                      abs(rst_total_euro - (latex_count_texteuro_latex + latex_count_euro_latex)), latex_path)
        exit(1)

logging.info("Check builds...OK!")
exit(0)

